/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 HttpLib is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HttpLib is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

package org.frknkrc44.httplib;

import java.io.*;
import java.net.*;
import java.util.*;

abstract class BaseRequest {
	
	protected BaseRequest(String link, Map<String,Object> args) throws IOException {
		onProcess(link, args);
	}
	
	protected static HttpURLConnection getConnector(String link) throws IOException {
		URL url = new URL(link);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("User-Agent","Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.7113.93 Safari/537.36");
		conn.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		conn.setRequestProperty("Accept-Language","tr-TR,tr;q=0.8,en-US;q=0.5,en;q=0.3");
		conn.setRequestProperty("Cache-Control","max-age=0");
		conn.setRequestProperty("Connection","keep-alive");
		conn.setRequestProperty("Referer","https://covid19.saglik.gov.tr/");
		conn.setRequestProperty("Upgrade-Insecure-Requests","1");

		conn.setDoInput(true);
		return conn;
	}
	
	protected abstract void onProcess(String link, Map<String, Object> args) throws IOException;
	protected abstract void onPostProcess(Map<String, List<String>> headers, String result);
	
	protected static String getCharset(Map<String, Object> args) {
		return args != null && args.containsKey(KEY_CHARSET) ? (String) args.get(KEY_CHARSET) : null;
	}
	
	protected static String getResult(HttpURLConnection conn) throws IOException {
		return getResult(conn, null);
	}
	
	protected static String getResult(HttpURLConnection conn, String charset) throws IOException {
		InputStreamReader isr = new InputStreamReader(conn.getInputStream(),charset == null ? "UTF-8" : charset);
		char[] buf = new char[1024];
		int read;
		StringBuilder builder = new StringBuilder();

		while((read = isr.read(buf,0,buf.length)) > 0){
			builder.append(buf,0,read);
		}

		return builder.toString();
	}
	
	protected String parseMethodArgs(Map<String, Object> args){
		if(args != null && args.size() > 0){
			List<String> keys = new ArrayList<String>(args.keySet());
			String out = "";
			for(int i = 0;i < args.size();i++){
				String key = keys.get(i);
				if(key.equals(KEY_CHARSET)){
					continue;
				}
				out += encodeArgs(key,args.get(key));
			}
			out = out.substring(0,out.length()-1);
			return out;
		}
		return "";
	}

	private String encodeArgs(String key, Object value){
		return key + "=" + URLEncoder.encode(value.toString()) + "&";
	}
	
	public static final String KEY_CHARSET = "Content-Charset";
	
}
