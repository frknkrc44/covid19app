/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 HttpLib is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 HttpLib is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.frknkrc44.httplib;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.Map;

public abstract class GetRequest extends BaseRequest {
	
	public GetRequest(String link) throws IOException {
		this(link, null);
	}
	
	public GetRequest(String link, Map<String, Object> args) throws IOException {
		super(link, args);
	}

	@Override
	public void onProcess(String link, Map<String, Object> args) throws IOException {
		String arg = parseMethodArgs(args);
		arg = arg.length() > 0 ? ("?" + arg) : "";
		HttpURLConnection conn = getConnector(link + arg);
		conn.connect();
		onPostProcess(conn.getHeaderFields(), getResult(conn,getCharset(args)));
	}
	
}
