/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 Covid19App is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Covid19App is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.frknkrc44.covid19;

import android.content.Context;
import android.util.*;
import android.view.Gravity;
import android.widget.*;
import android.content.res.Resources;

public class CovidDataItem extends LinearLayout {
	private final boolean hideDate;
	
	private CovidDataItem(Context context){
		super(context);
		throw new RuntimeException("Please use (Context, CovidDataModel) or (Context, CovidDataModel, boolean) constructor");
	}
	
	private CovidDataItem(Context context, AttributeSet attrs){
		this(context);
	}
	
	private CovidDataItem(Context context, AttributeSet attrs, int defStyleAttr){
		this(context);
	}
	
	private CovidDataItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes){
		this(context);
	}
	
	public CovidDataItem(Context context, BaseDataModel model){
		this(context, model, false);
	}
	
	public CovidDataItem(Context context, BaseDataModel model, boolean hideDate){
		super(context);
		this.hideDate = hideDate;
		setOrientation(VERTICAL);
		createFromDataModel(model);
	}
	
	private void createFromDataModel(BaseDataModel baseModel){
		if(baseModel == null){
			return;
		}
		
		if(baseModel instanceof CovidDataModel){
			CovidDataModel model = (CovidDataModel) baseModel;
			if(!hideDate)
				addSubView("Tarih", model.tarih, "");
			addSubView("Günlük test", model.gun_test);
			addSubView("Günlük vaka", model.gun_vaka);
			addSubView("Günlük hasta", model.gun_hasta);
			addSubView("Günlük ölüm", model.gun_olum);
			addSubView("Günlük iyileşen", model.gun_iyi);
			addSubView("Toplam test", model.top_test);
			addSubView("Toplam hasta", model.top_vaka);
			addSubView("Toplam ölüm", model.top_olum);
			addSubView("Toplam iyileşen", model.top_iyi);
			addSubView("Yoğun bakım", model.top_yb);
			addSubView("Entübe", model.top_etb);
			addSubView("Ağır hasta", model.agir);
			addSubView("Zatürre oranı", model.zaturre, "^", "%");
			addSubView("Yatak doluluk oranı", model.yatak_doluluk, "^", "%");
			addSubView("Yb. doluluk oranı", model.yb_doluluk, "^", "%");
			addSubView("Ventilatör doluluk oranı", model.vt_doluluk,  "^", "%");
			addSubView("Filyasyon oranı", model.fly_oran, "^", "%");
			addSubView("Ort. filyasyon süresi", model.ort_fly, "$", " SAAT");
			addSubView("Ort. temaslı tespit süresi", model.ort_tem_tespit, "$", " SAAT");
		} else if(baseModel instanceof CovidVaccineDataModel){
			CovidVaccineDataModel model = (CovidVaccineDataModel) baseModel;
			if(!hideDate)
				addSubView("Güncelleme saati", model.asiDozuGuncellemeSaati);
			addSubView("Doz 1 aşı sayısı", model.doz1AsiSayisi);
			addSubView("Doz 2 aşı sayısı", model.doz2AsiSayisi);
			addSubView("Doz 3 aşı sayısı", model.doz3AsiSayisi);
			addSubView("Doz 4 aşı sayısı", model.doz4AsiSayisi);
			addSubView("Toplam aşı dozu sayısı", model.toplamAsiDozuSayisi);
			addSubView("Günlük aşı dozu sayısı", model.gunlukAsiDozuSayisi);
			addSubView("Doz 1 Türkiye ortalaması", model.dozTurkiyeOrtalamasi, "^", "%");
			addSubView("Doz 2 Türkiye ortalaması", model.doz2TurkiyeOrtalamasi, "^", "%");
			addSubView("Doz 3 Türkiye ortalaması", model.doz3TurkiyeOrtalamasi, "^", "%");
			addSubView("Doz 4 Türkiye ortalaması", model.doz4TurkiyeOrtalamasi, "^", "%");
		}
	}
	
	private void addSubView(String key, String value){
		addSubView(key, value, "\\.");
	}
	
	private void addSubView(String key, String value, String replace){
		addSubView(key, value, replace, "");
	}
	
	private void addSubView(String key, String value, String replace, String replaceAfter){
		if(value.trim().length() < 1)
			return;
		addView(getSubView(key, value, replace, replaceAfter));
	}
	
	private LinearLayout getSubView(String key, String value, String replace, String replaceAfter){
		float dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, Resources.getSystem().getDisplayMetrics());
		LinearLayout layout = new LinearLayout(getContext());
		layout.setPadding((int) dp, 0, (int) dp, 0);
		LayoutParams params = new LayoutParams(-1, -2);
		layout.setLayoutParams(params);
		TextView text1 = new TextView(getContext());
		params = new LayoutParams(-2, -2, 0);
		params.setMargins(0, 0, (int) dp, 0);
		text1.setLayoutParams(params);
		text1.setText(key);
		// text1.setTextSize(dp / 2);
		layout.addView(text1);
		TextView text2 = new TextView(getContext());
		text2.setGravity(Gravity.END);
		params = new LayoutParams(-1, -2, 1);
		text2.setLayoutParams(params);
		text2.setText(replace.length() > 0 ? value.replaceAll(replace, replaceAfter) : value);
		// text2.setTextSize(dp / 2);
		layout.addView(text2);
		return layout;
	}
	
}
