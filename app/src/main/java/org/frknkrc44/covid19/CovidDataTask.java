/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 Covid19App is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Covid19App is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.frknkrc44.covid19;

import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.frknkrc44.httplib.GetRequest;
import org.json.JSONArray;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class CovidDataTask extends AsyncTask<String, Void, List<BaseDataModel>> {
	private static String ret = "";

	@Override
	protected List<BaseDataModel> doInBackground(String[] p1){
		List<BaseDataModel> modelList = new ArrayList<>();
		int try1Count = 0, try2Count = 0;
		XmlPullParser parser = null;
		
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			parser = factory.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
		} catch(Throwable t){
			throw new RuntimeException(t);
		}
		
		try {
			String link = "https://covid19.saglik.gov.tr/";
			while(modelList.size() < 1 && try1Count++ < 5){
				if(try1Count > 1){
					Thread.sleep(try1Count * 250);
				}
				new CovidRequest(link);
				do {
					// do nothing
				} while(ret == null);
				ByteArrayInputStream bais = new ByteArrayInputStream(ret.getBytes());
				parser.setInput(bais, null);
				String data = getJson(parser, "var doz1asisayisi");
				modelList.add(CovidVaccineDataModel.fromJSData(data));
				bais.close();
			}
		} catch(Throwable t){
			throw new RuntimeException(t);
		}
		
		ret = null;
		
		try {
			String link = "https://covid19.saglik.gov.tr/TR-66935/genel-koronavirus-tablosu.html";
			while(modelList.size() < 2 && try2Count++ < 5){
				if(try2Count > 1){
					Thread.sleep(try2Count * 250);
				}
				fillModelList(link, parser, modelList);
			}
			return modelList;
		} catch(Throwable t){
			throw new RuntimeException(t);
		}
	}
	
	private void fillModelList(String link, XmlPullParser parser, List<BaseDataModel> modelList) throws Throwable {
		new CovidRequest(link);
		do {
			// do nothing
		} while(ret == null);
		
		ByteArrayInputStream bais = new ByteArrayInputStream(ret.getBytes());
		parser.setInput(bais, null);
		String data = getJson(parser, "var geneldurumjson");
		bais.close();
		ret = data.replaceAll("(//|<!\\[CDATA\\[|]]>|;|var geneldurumjson =|\\n|\\s)","");
		JSONArray ja = new JSONArray(ret);
		for(int i = 0;i < ja.length();i++){
			modelList.add(CovidDataModel.fromJson(ja.getJSONObject(i)));
		}
		bais.close();
	}
	
	private String getJson(XmlPullParser parser, String term) throws XmlPullParserException {
		int eventType = parser.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT){
			try {
				if(eventType == XmlPullParser.START_TAG && parser.getName().equals("script")){
					String text = parser.nextText();
					if(text != null && text.contains(term)){
						return text;
					}
				}
				eventType = parser.next();
			} catch(Throwable t){
				// do nothing
			}
		}
		return "[]";
	}

	@Override
	protected void onPostExecute(List<BaseDataModel> result){
		if(listener != null)
			listener.onFinish(result);
		super.onPostExecute(result);
	}
	
	public void setOnFinishListener(OnFinishListener listener){
		this.listener = listener;
	}
	
	private OnFinishListener listener;
	
	public static interface OnFinishListener {
		void onFinish(List<BaseDataModel> out);
	}
	
	private class CovidRequest extends GetRequest {
		
		private CovidRequest(String link) throws IOException {
			super(link, null);
		}
		
		@Override
		protected void onPostProcess(Map<String, List<String>> headers, String result){
			ret = result;
		}
		
	}
	
}
