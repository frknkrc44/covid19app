/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 Covid19App is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Covid19App is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.frknkrc44.covid19;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.*;
import java.util.List;

public class MainActivity extends Activity implements CovidDataTask.OnFinishListener {

	@Override
	public void onFinish(List<BaseDataModel> out){
		setContentView(new CovidDataListView(this, out));
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onRestart();
		FrameLayout.LayoutParams fp = new FrameLayout.LayoutParams(-2, -2);
		fp.gravity = Gravity.CENTER;
		ProgressBar spinner = new ProgressBar(this);
		spinner.setLayoutParams(fp);
		setContentView(spinner);
    }
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState){
		super.onPostCreate(savedInstanceState);
		CovidDataTask task = new CovidDataTask();
		task.setOnFinishListener(this);
		task.execute();
	}
	
}
