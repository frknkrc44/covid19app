/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 Covid19App is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Covid19App is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.frknkrc44.covid19;

public class CovidVaccineDataModel extends BaseDataModel {
	public final String doz1AsiSayisi, doz2AsiSayisi, doz3AsiSayisi, doz4AsiSayisi, toplamAsiDozuSayisi, gunlukAsiDozuSayisi, dozTurkiyeOrtalamasi, doz2TurkiyeOrtalamasi, doz3TurkiyeOrtalamasi, doz4TurkiyeOrtalamasi, asiDozuGuncellemeSaati;
	
	public CovidVaccineDataModel(String doz1AsiSayisi, String doz2AsiSayisi, String doz3AsiSayisi, String doz4AsiSayisi, String toplamAsiDozuSayisi, String gunlukAsiDozuSayisi, String dozTurkiyeOrtalamasi, String doz2TurkiyeOrtalamasi, String doz3TurkiyeOrtalamasi, String doz4TurkiyeOrtalamasi, String asiDozuGuncellemeSaati){
		this.doz1AsiSayisi = doz1AsiSayisi;
		this.doz2AsiSayisi = doz2AsiSayisi;
		this.doz3AsiSayisi = doz3AsiSayisi;
		this.doz4AsiSayisi = doz4AsiSayisi;
		this.toplamAsiDozuSayisi = toplamAsiDozuSayisi;
		this.gunlukAsiDozuSayisi = gunlukAsiDozuSayisi;
		this.dozTurkiyeOrtalamasi = dozTurkiyeOrtalamasi;
		this.doz2TurkiyeOrtalamasi = doz2TurkiyeOrtalamasi;
		this.doz3TurkiyeOrtalamasi = doz3TurkiyeOrtalamasi;
		this.doz4TurkiyeOrtalamasi = doz4TurkiyeOrtalamasi;
		this.asiDozuGuncellemeSaati = asiDozuGuncellemeSaati;
	}
	
	public static CovidVaccineDataModel fromJSData(String data){
		String doz1AsiSayisi = "", doz2AsiSayisi = "", doz3AsiSayisi = "", doz4AsiSayisi = "", toplamAsiDozuSayisi = "", gunlukAsiDozuSayisi = "", dozTurkiyeOrtalamasi = "", doz2TurkiyeOrtalamasi = "", doz3TurkiyeOrtalamasi = "", doz4TurkiyeOrtalamasi = "", asiDozuGuncellemeSaati = "";
		for(String line : data.split("\\n")){
			line = line.replace("var ", "");
			if(line.contains("=")){
				String[] split = line.split("=");
				String name = split[0].trim();
				String value = split[1].substring(split[1].indexOf("'")+1, split[1].lastIndexOf("'"));
				switch(name){
					case "doz1asisayisi":
						doz1AsiSayisi = value;
						break;
					case "doz2asisayisi":
						doz2AsiSayisi = value;
						break;
					case "doz3asisayisi":
						doz3AsiSayisi = value;
						break;
					case "doz4asisayisi":
						doz4AsiSayisi = value;
						break;
					case "toplamasidozusayisi":
						toplamAsiDozuSayisi = value;
						break;
					case "gunluksidozusayisi":
						gunlukAsiDozuSayisi = value;
						break;
					case "dozturkiyeortalamasi":
						dozTurkiyeOrtalamasi = value;
						break;
					case "doz2turkiyeortalamasi":
						doz2TurkiyeOrtalamasi = value;
						break;
					case "doz3turkiyeortalamasi":
						doz3TurkiyeOrtalamasi = value;
						break;
					case "doz4turkiyeortalamasi":
						doz4TurkiyeOrtalamasi = value;
						break;
					case "asidozuguncellemesaati":
						asiDozuGuncellemeSaati = value;
				}
			}
		}
		
		return new CovidVaccineDataModel(doz1AsiSayisi, doz2AsiSayisi, doz3AsiSayisi, doz4AsiSayisi, toplamAsiDozuSayisi, gunlukAsiDozuSayisi, dozTurkiyeOrtalamasi, doz2TurkiyeOrtalamasi, doz3TurkiyeOrtalamasi, doz4TurkiyeOrtalamasi, asiDozuGuncellemeSaati);
	}
}
