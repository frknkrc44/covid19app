/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 Covid19App is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Covid19App is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.frknkrc44.covid19;

import org.json.*;

public class CovidDataModel extends BaseDataModel {
	public final String tarih, gun_test, gun_hasta, gun_vaka, gun_olum, gun_iyi, top_test, top_vaka, top_olum, top_iyi, top_yb, top_etb, zaturre, agir, yatak_doluluk, yb_doluluk, vt_doluluk, ort_fly, ort_tem_tespit, fly_oran;
	
	public CovidDataModel(String tarih, String gun_test, String gun_hasta, String gun_vaka, String gun_olum, String gun_iyi, String top_test, String top_vaka, String top_olum, String top_iyi, String top_yb, String top_etb, String zaturre, String agir, String yatak_doluluk, String yb_doluluk, String vt_doluluk, String ort_fly, String ort_tem_tespit, String fly_oran) {
		this.tarih = tarih;
		this.gun_test = gun_test;
		this.gun_hasta = gun_hasta;
		this.gun_vaka = gun_vaka;
		this.gun_olum = gun_olum;
		this.gun_iyi = gun_iyi;
		this.top_test = top_test;
		this.top_vaka = top_vaka;
		this.top_olum = top_olum;
		this.top_iyi = top_iyi;
		this.top_yb = top_yb;
		this.top_etb = top_etb;
		this.zaturre = zaturre;
		this.agir = agir;
		this.yatak_doluluk = yatak_doluluk;
		this.yb_doluluk = yb_doluluk;
		this.vt_doluluk = vt_doluluk;
		this.ort_fly = ort_fly;
		this.ort_tem_tespit = ort_tem_tespit;
		this.fly_oran = fly_oran;
	}
	
	public static CovidDataModel fromJson(JSONObject obj) throws JSONException {
		String tarih = obj.getString("tarih");
		String gun_test = obj.getString("gunluk_test");
		String gun_hasta = obj.getString("gunluk_hasta");
		String gun_vaka = obj.getString("gunluk_vaka");
		String gun_olum = obj.getString("gunluk_vefat");
		String gun_iyi = obj.getString("gunluk_iyilesen");
		String top_test = obj.getString("toplam_test");
		String top_vaka = obj.getString("toplam_hasta");
		String top_olum = obj.getString("toplam_vefat");
		String top_iyi = obj.getString("toplam_iyilesen");
		String top_yb = obj.getString("toplam_yogun_bakim");
		String top_etb = obj.getString("toplam_entube");
		String zaturre = obj.getString("hastalarda_zaturre_oran");
		String agir = obj.getString("agir_hasta_sayisi");
		String yatak_doluluk = obj.getString("yatak_doluluk_orani");
		String yb_doluluk = obj.getString("eriskin_yogun_bakim_doluluk_orani");
		String vt_doluluk = obj.getString("ventilator_doluluk_orani");
		String ort_fly = obj.getString("ortalama_filyasyon_suresi");
		String ort_tem_tespit = obj.getString("ortalama_temasli_tespit_suresi");
		String fly_oran = obj.getString("filyasyon_orani");
		return new CovidDataModel(tarih, gun_test, gun_hasta, gun_vaka, gun_olum, gun_iyi, top_test, top_vaka, top_olum, top_iyi, top_yb, top_etb, zaturre, agir, yatak_doluluk, yb_doluluk, vt_doluluk, ort_fly, ort_tem_tespit, fly_oran);
	}
}
