/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 Covid19App is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Covid19App is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.frknkrc44.covid19;

import android.database.DataSetObserver;
import android.view.*;
import android.widget.*;
import java.util.List;

public class CovidDataListAdapter implements ExpandableListAdapter {
	private final List<BaseDataModel> models;
	
	public CovidDataListAdapter(List<BaseDataModel> models){
		if(models == null){
			throw new RuntimeException("models cannot be null");
		}
		this.models = models;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver p1){}

	@Override
	public void unregisterDataSetObserver(DataSetObserver p1){}

	@Override
	public int getGroupCount(){
		return models.size();
	}

	@Override
	public int getChildrenCount(int p1){
		return 1;
	}

	@Override
	public Object getGroup(int p1){
		return models.get(p1);
	}

	@Override
	public Object getChild(int p1, int p2){
		return getGroup(p1);
	}

	@Override
	public long getGroupId(int p1){
		return p1;
	}

	@Override
	public long getChildId(int p1, int p2){
		return getGroupId(p1);
	}

	@Override
	public boolean hasStableIds(){
		return false;
	}

	@Override
	public View getGroupView(int p1, boolean p2, View p3, ViewGroup p4){
		View group = LayoutInflater.from(p4.getContext()).inflate(android.R.layout.simple_expandable_list_item_1, p4, false);
		TextView text = group.findViewById(android.R.id.text1);
		BaseDataModel model = models.get(p1);
		if(model instanceof CovidDataModel){
			text.setText(((CovidDataModel) model).tarih);
		} else if(model instanceof CovidVaccineDataModel){
			String vaccineStats = group.getResources().getString(R.string.vaccine_stats);
			vaccineStats += " - " + ((CovidVaccineDataModel) model).asiDozuGuncellemeSaati;
			text.setText(vaccineStats);
		}
		
		return group;
	}

	@Override
	public View getChildView(int p1, int p2, boolean p3, View p4, ViewGroup p5){
		return new CovidDataItem(p5.getContext(), models.get(p1), true);
	}

	@Override
	public boolean isChildSelectable(int p1, int p2){
		return false;
	}

	@Override
	public boolean areAllItemsEnabled(){
		return false;
	}

	@Override
	public boolean isEmpty(){
		return models.size() < 1;
	}

	@Override
	public void onGroupExpanded(int p1){}

	@Override
	public void onGroupCollapsed(int p1){}

	@Override
	public long getCombinedChildId(long p1, long p2){
		return getChildId((int) p1, (int) p2);
	}

	@Override
	public long getCombinedGroupId(long p1){
		return getGroupId((int) p1);
	}

}
