/*
 Copyright (C) 2020 Furkan Karcıoğlu.

 Covid19App is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Covid19App is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.frknkrc44.covid19;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.*;
import java.util.List;

public class CovidDataListView extends ExpandableListView implements OnGroupCollapseListener, OnGroupExpandListener {
	private CovidDataListAdapter adapter;
	private int expandIndex = -1;
	
	private CovidDataListView(Context context){
		super(context);
		throw new RuntimeException("Please use (Context context, List<CovidDataModel> models) constructor");
	}

	private CovidDataListView(Context context, AttributeSet attrs){
		this(context);
	}

	private CovidDataListView(Context context, AttributeSet attrs, int defStyleAttr){
		this(context);
	}

	private CovidDataListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes){
		this(context);
	}
	
	public CovidDataListView(Context context, List<BaseDataModel> models){
		super(context);
		adapter = new CovidDataListAdapter(models);
		setAdapter(adapter);
		setOnGroupExpandListener(this);
		setOnGroupCollapseListener(this);
		if(!adapter.isEmpty()){
			expandGroup(0);
		}
	}
	
	@Override
	public void onGroupCollapse(int p1){
		expandIndex = -1;
	}
	
	@Override
	public void onGroupExpand(int p1){
		if(expandIndex >= 0 && isGroupExpanded(expandIndex)){
			collapseGroup(expandIndex);
		}
		expandIndex = p1;
	}
}
